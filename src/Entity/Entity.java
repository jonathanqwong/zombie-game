/*
 * Collision Logic REFERENCED FROM ASSIGNMENT_10_COLLISIONBOX BY JOHN ABDOU
 */

package Entity;

import javafx.animation.Transition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

/**
**  In the sprite class, every object in the game has properties.
** All game entities/object belongs in a pane, has an image object,
** isRemovable, isMoveable, and has dimensions (only for walls).
**/
public class Entity extends Transition {
	
	// Every entity/object's properties belongs in a pane, can have an image, 
	// directions(dx, dy), dimensions (x,y,w,h)
	// Wall parameters - (Pane, layer, x, y, dx, dy, w, h)
	double x;
	double y;
	double dx;
	double dy;
	double w;
	double h;
	boolean isRemovable = false;
	boolean isMovable = true;
	
	ImageView imageView;
	Image image;
	Pane pane;
	Rectangle wallSprite;
	
	//Entity method 
	public Entity (Pane pane, Image image, double x, double y, double dx, double dy, double w, double h) {
		this.pane = pane;
		this.image = image;
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.w = w;
		this.h = h;
		this.imageView = new ImageView(image);
		this.imageView.relocate(x, y);
		
		wallSprite = new Rectangle(x, y, w, h);
		
		addEntity();

	} //end of Entity method
	
	public void addEntity() {
		this.pane.getChildren().add(this.imageView);
	}
	
	public void removeEntity() {
		this.pane.getChildren().remove(this.imageView);
	}
	
	public boolean isRemovable() {
		return isRemovable;
	}
	
	public void setRemovable(boolean isRemovable) {
		this.isRemovable = isRemovable;
	}
	
	public boolean isMovable() {
		return isMovable;
	}
	
	public void setMovable(boolean isMovable) {
		this.isMovable = isMovable;
	}
	
	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getDx() {
		return dx;
	}
	
	public void setDx(double dx) {
		this.dx = dx;
	}
	
	public double getDy() {
		return dy;
	}
	
	public void setDy(double dy) {
		this.dy = dy;
	}
	
	public double getW() {
		return w;
	}
	
	public void setW(double w) {
		this.w = w;
	}
	
	public double getH() {
		return h;
	}
	
	public void setH(double h) {
		this.h = h;
	}
	
	public Rectangle getRectangle() {
		return wallSprite;
	}
	
	public void move() {
		if(!isMovable) 
		{
			//System.out.println("NOT MOVING");
			return;
		}
			
		else
		{
			//System.out.println("MOVING");
			x += dx;
			y += dy;
			imageView.relocate(x, y);
			wallSprite.setX(x);
			wallSprite.setY(y);
		}
	}
	
	public boolean isCollidingWithRobots(Entity robots) {
		return((wallSprite.intersects(
				robots.getRectangle().getX(),
				robots.getRectangle().getY(),
				robots.getRectangle().getWidth(),
				robots.getRectangle().getHeight())));
	}
	
	public boolean isCollidingWithWall(Walls wall) {
		return((wallSprite.intersects(
				wall.getRectangle().getX(),
				wall.getRectangle().getY(),
				wall.getRectangle().getWidth(),
				wall.getRectangle().getHeight())));
	}
	
	@Override
	protected void interpolate(double arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
