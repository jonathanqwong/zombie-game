package Entity;

import java.util.Random;

import BerzerkPKG.SpriteAnimation;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Zombie extends Entity {
	
	/**
	** Zombie Entity variable declarations
	**/
	//Zombie image file variables
    Image zombieImage = new Image("images/ZombieRight.png"); //default image for robot to Walk right
    Image zombieImage1 = new Image("images/ZombieRight.png"); //walk right
    Image zombieImage2 = new Image("images/ZombieLeft.png"); //walk left
    Image zombieImage3 = new Image("images/ZombieUp.png"); //walk up
    Image zombieImage4 = new Image("images/ZombieDown.png"); //walk down
    Image robotDyingImage = new Image("images/Dying.png"); //robot dying
	
    //Zombie variables for Sprite frames
    private static int zombieColumns = 4; // number of columns in robot sprite image
    private static int zombieCount = 4;   // 4 frames of robot walking up
    private int zombieOffset_X = 0; // offset X of where the robot sprite image starts in sprite sheet.
    private int zombieOffset_Y = 0; // offset Y of where the robot sprite image starts in sprite sheet.
    private static int zombieWidth = 20;  // width of robot sprite
    private static int zombieHeight = 32; // height of robot sprite
 
    //Boolean variables for entity facing direction
    boolean isWalkRight = true; //default for player to Walk right
    boolean isWalkLeft = false;
    boolean isWalkUp = false;
    boolean isWalkDown = false;
    boolean isShooting = false;
    
    //Declaration of KeyHandler class
	double zombieMoveSpeed;
	Random dirInt = new Random();
    
    /** 
     * Zombie method to create zombie
     * Zombie constructor to invoke the Entity class 
     * parameters include:
     * @param pane
     * @param image
     * @param x
     * @param y
     * @param dx
     * @param dy
     * @param w
     * @param h
     * @param speed
     */
	public Zombie(Pane pane, Image image, double x, double y, double dx, double dy, double w, double h, double zombieMoveSpeed) {
		
		super(pane, image, x, y, dx, dy, w, h);
		this.zombieMoveSpeed = zombieMoveSpeed;
		setMovable(true);
		this.imageView.setViewport(new Rectangle2D(zombieOffset_X, zombieOffset_Y, zombieWidth, zombieHeight));
        final Animation zombieAnimation = new SpriteAnimation(this.imageView,Duration.millis(900),zombieCount, zombieColumns, zombieOffset_X, zombieOffset_Y,zombieWidth, zombieHeight);
        zombieAnimation.setCycleCount(Animation.INDEFINITE);
        zombieAnimation.play();
	
	} //end of robot method
	
    /** 
     ** zombieRandomMove method to move zombie - 
     ** use random method to move zombie and boolean for Walk direction
     **/
	public void zombieRandomMove() {
		
		//add one zombie every direction
		int tempInt = dirInt.nextInt(20) + 1;
		
		//zombie right - default position
		if(tempInt == 1) {
			dx = zombieMoveSpeed; //move in +dx direction
			this.imageView.setImage(zombieImage1);
			isWalkRight = true; 
			isWalkLeft = false;
		
		//zombie left
		} else if (tempInt == 2) {
			dx = -zombieMoveSpeed; //move in -dx direction
			this.imageView.setImage(zombieImage2);
			isWalkUp = false;
			isWalkDown = true;
		
		//zombie up
		} else if(tempInt == 3 ) {
			dy = -zombieMoveSpeed; //move in -dy direction
			this.imageView.setImage(zombieImage3);
			isWalkUp = true;
			isWalkDown = false;
		
		//zombie down
		} else if(tempInt == 4) {
			dy = zombieMoveSpeed; //move in +dy direction
			this.imageView.setImage(zombieImage4);
			isWalkUp = false ;
			isWalkDown = true;
		
		} else {
			
			dx = 0;
			dy = 0;
			isWalkRight = true;
			isWalkLeft = false;
			isWalkUp = false;
			isWalkDown = false;
		
		} 
		
	} //end of randomZombieMove method
	
	
    //playZombieDeathAnimation method to show player dying image for collision logic
	public void playZombieDeathAnimation(Entity zombieEntity) {
		//this.imageView.setImage(zombieDyingImage);
		this.imageView.setViewport(new Rectangle2D(zombieOffset_X, zombieOffset_Y, zombieWidth, zombieHeight));
        final Animation playZombieDeathAnimation = new SpriteAnimation(
        		this.imageView, Duration.millis(200),
        		zombieColumns, zombieColumns,
        		zombieOffset_X, zombieOffset_Y,
        		zombieWidth, zombieHeight);
        playZombieDeathAnimation.setCycleCount(1);
        playZombieDeathAnimation.play();
        //System.out.println("Collided");
        //robotEntity.removeEntity();
		
	} //end of playRobotDeathAnimation
	
	public String getDirection()
	{
		String dir = "a";
		if(isWalkUp && isWalkRight)
			dir = "UP_RIGHT";
		else if(isWalkUp && isWalkLeft)
			dir = "UP_LEFT";
		else if(isWalkDown && isWalkRight)
			dir = "DOWN_RIGHT";
		else if(isWalkDown && isWalkLeft)
			dir = "DOWN_LEFT";
		else if(isWalkUp && (!isWalkRight && !isWalkLeft))
			dir = "UP";
		else if(isWalkDown && (!isWalkRight && !isWalkLeft))
			dir = "DOWN";
		else if(isWalkRight)
			dir = "RIGHT";
		else if(isWalkLeft)
			dir = "LEFT";
		return dir;
	}
	
	public boolean isShooting() {
		Random ranGen = new Random();
		int ranNum = ranGen.nextInt(600)+1;
		if(ranNum == 10)
		return true;
		return false;
	}
	
	public void move() {
		super.move();
	}

}
