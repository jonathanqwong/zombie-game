package Entity;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class Walls extends Entity {
	
    /** 
     * Walls method to create walls
     * Walls constructor to invoke the Sprite class 
     * parameters include:
     * @param pane
     * @param image
     * @param x
     * @param y
     * @param dx
     * @param dy
     * @param w
     * @param h
     */
	
	public Walls(Pane pane, Image image, double x, double y, double dx, double dy, double w, double h) {
		super(pane, image, x, y, dx, dy, w, h);
	}
	
	public boolean isMovable() {
		return false;
	}
	
	public boolean isRemovable() {
		return false;
	}
}