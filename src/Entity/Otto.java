package Entity;

import BerzerkPKG.SpriteAnimation;

import java.util.Random;

import javafx.animation.Animation;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Otto extends Entity {

	/**
	** Otto Entity variable declarations
	**/
	
	//otto file variables
    Image OttoImage = new Image("images/Otto.png"); //default image for Otto

    // setup variables for Otto.
    private int Otto_columns = 1;	// number of columns in sprite image
    private int Otto_count = 1;		// number of frames for sprite
    private int Otto_offset_X = 0;	// offset X of where the image starts
    private int Otto_offset_Y = 0;	// offset Y of where the image starts
    private int Otto_width = 50;		// width of the sprite
    private int Otto_height = 50;	// height of the sprite
	
    Random dirInt = new Random();
    double ottoMoveSpeed;
    
    /** 
     * Otto method to create otto
     * Otto constructor to invoke the Entity class 
     * parameters include:
     * @param pane
     * @param image
     * @param x
     * @param y
     * @param dx
     * @param dy
     * @param w
     * @param h
     * @param speed
     */
    
	public Otto(Pane pane, Image image, double x, double y, double dx, double dy, double w, double h, double ottoMoveSpeed) {
		super(pane, image, x, y, dx, dy, w, h);
		this.ottoMoveSpeed = ottoMoveSpeed;
		setMovable(true);
		this.imageView.setViewport(new Rectangle2D(Otto_offset_X,Otto_offset_Y, Otto_width, Otto_height));
		final Animation otto_animation = new SpriteAnimation(
				this.imageView, Duration.millis(900),
				Otto_count, Otto_columns,
				Otto_offset_X, Otto_offset_Y,
				Otto_width, Otto_height);
		otto_animation.setCycleCount(Animation.INDEFINITE);
		otto_animation.play();
	}
	
	public void ottoChase(Player player)
	{
		if(this.getX() < player.getX()){
			dx = ottoMoveSpeed;
		}
		if(this.getY() > player.getY()){
			dy = -ottoMoveSpeed;
		}
		if(this.getY() < player.getY()){
			dy = ottoMoveSpeed;
		}
		if(this.getX() > player.getX()){
			dx = -ottoMoveSpeed;
		}
	}
	
	public boolean isMovable()
	{
		return true;
	}
	
	public void move()
	{
		super.move();
	}
	
} //end of Otto Class
