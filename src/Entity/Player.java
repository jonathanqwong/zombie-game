package Entity;

import java.util.Random;

import javafx.animation.Animation;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import BerzerkPKG.KeyInputHandler;
import BerzerkPKG.SpriteAnimation;

public class Player extends Entity {
    
	/**
	** Player Entity variable declarations
	**/
	
	//Player file variables
    Image playerImage = new Image("images/WalkRight.png"); //default image for play to face right
    Image playerImage1 = new Image("images/WalkRight.png");
    Image playerImage2 = new Image("images/WalkLeft.png");
    Image playerDyingImage = new Image("images/Dying.png");
	
    //Player variables for Sprite frames
    private static int Pcolumns = 4;	// number of columns in sprite image
    private static int Pcount = 4;		// number of frames for sprite
    private int Poffset_X = 0;	// offset X of where the image starts
    private int Poffset_Y = 0;	// offset Y of where the image starts
    private static int Pwidth = 20;	// width of the sprite
    private static int Pheight = 32;	// height of the sprite
    
    private static int P_Dying_columns = 6;	// number of columns in sprite image
    private static int P_Dying_count = 6;		// number of frames for sprite
    private int P_Dying_offset_X = 0;	// offset X of where the image starts
    private int P_Dying_offset_Y = 0;	// offset Y of where the image starts
    private static int P_Dying_width = 26;	// width of the sprite
    private static int P_Dying_height = 40;	// height of the sprite
    
    //Boolean variables for entity facing direction
    boolean isWalkRight = true; //default for player to face right
    boolean isWalkLeft = false;
    boolean isWalkUp = false;
    boolean isWalkDown = false;
    boolean isShooting = false;   
  
    //declaration of KeyHandler class
    KeyInputHandler playerInput;
	double playerMoveSpeed;
	
    /**
     * Player method to create a robot
     * player constructor to invoke the Entity class 
     * parameters include:
     * @param pane
     * @param image
     * @param x
     * @param y
     * @param dx
     * @param dy
     * @param w
     * @param h
     * @param speed
     * @param input
     */
    public Player(Pane pane, Image image, double x, double y, double dx, double dy, double w, double h, double playerSpeed, KeyInputHandler input) {
		
		super(pane, image, x, y, dx, dy, w, h);
		playerInput = input;
		playerMoveSpeed = playerSpeed;
		setMovable(true);
		this.imageView.setViewport(new Rectangle2D(Poffset_X, Poffset_Y, Pwidth, Pheight));
        final Animation PlayerAnimation = new SpriteAnimation(
        		this.imageView, Duration.millis(700),
        		Pcount, Pcolumns,Poffset_X, 
        		Poffset_Y,Pwidth, Pheight);
        PlayerAnimation.setCycleCount(Animation.INDEFINITE);
        PlayerAnimation.play();
        
	} //end of player method

	/** 
     ** playerInputMove method to control player - 
     ** use selection to decide which direction player is moving(boolean for Walk direction)
     */
    
    public void playerInputMove() {
    	//Player up
    	if(playerInput.isMoveUp()) {
    		dy = -playerMoveSpeed;
    		System.out.println("UP");
    	    isWalkRight = false; //default for player to Walk right
    	    isWalkLeft = false;
    	    isWalkUp = true;
    	    isWalkDown = false;
    	
    	//Player down
    	}	else if(playerInput.isMoveDown()) {
    		
    		dy = playerMoveSpeed;
    		System.out.println("DOWN");
    	    isWalkRight = false; //default for player to Walk right
    	    isWalkLeft = false;
    	    isWalkUp = false;
    	    isWalkDown = true;
    	
    	}	else {
    		
    		dy = 0;
    		isWalkUp = false;
    		isWalkDown = false;
    	}
    	
    	//Player Right - default position
    	if(playerInput.isMoveRight()) {
    		this.imageView.setImage(playerImage1);
    		System.out.println("RIGHT");
    		dx = playerMoveSpeed; //move in dx direction
    	    isWalkRight = true; //default for player to Walk right
    	    isWalkLeft = false;
    	
    	//Player Left    
    	} 	else if(playerInput.isMoveLeft()) {
    		
    		this.imageView.setImage(playerImage2);
    		System.out.println("LEFT"); //
    	    dx = -playerMoveSpeed; //move in -dx direction
    	    isWalkRight = false; //default for player to Walk right
    	    isWalkLeft = true;
    	
    	}	else {
    		
    		dx = 0;
    		isWalkRight = false;
    		isWalkLeft = false;
    	}
    	
    } //end of playerInput method
    
    //playPlayerDeathAnimation method to show player dying image for collision logic
	public void playPlayerDeathAnimation(Entity player) {
		
		setMovable(false);
		this.imageView.setImage(playerDyingImage);
		this.imageView.setViewport(new Rectangle2D(P_Dying_offset_X, P_Dying_offset_Y, P_Dying_width, P_Dying_height));
        final Animation playPlayerDeathAnimation = new SpriteAnimation(
        		this.imageView, Duration.millis(200),
        		P_Dying_count, P_Dying_columns,
        		P_Dying_offset_X, P_Dying_offset_Y,
        		P_Dying_width, P_Dying_height);
        playPlayerDeathAnimation.setCycleCount(1);
        playPlayerDeathAnimation.play();
        removeEntity();
        //System.out.println("Collided");
		try {
		    Thread.sleep(1250); 
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
        player.addEntity();
        setMovable(true);
		this.imageView.setImage(playerImage1);
		this.imageView.setViewport(new Rectangle2D(Poffset_X, Poffset_Y, Pwidth, Pheight));
		this.imageView.setLayoutX(110);
		this.imageView.setLayoutY(450);
		final Animation PlayerAnimation = new SpriteAnimation(
        		this.imageView, Duration.millis(700),
        		Pcount, Pcolumns,Poffset_X, 
        		Poffset_Y,Pwidth, Pheight);
        PlayerAnimation.setCycleCount(Animation.INDEFINITE);
        PlayerAnimation.play();

	} //end of playPlayerDeathAnimation

    
    //getDirection method for shooting
    public String getDirection()
	{
		String dir = "a";
		if(isWalkUp && isWalkRight)
			dir = "UP_RIGHT";
		else if(isWalkUp && isWalkLeft)
			dir = "UP_LEFT";
		else if(isWalkDown && isWalkRight)
			dir = "DOWN_RIGHT";
		else if(isWalkDown && isWalkLeft)
			dir = "DOWN_LEFT";
		else if(isWalkUp && (!isWalkRight && !isWalkLeft))
			dir = "UP";
		else if(isWalkDown && (!isWalkRight && !isWalkLeft))
			dir = "DOWN";
		else if(isWalkRight)
			dir = "RIGHT";
		else if(isWalkLeft)
			dir = "LEFT";
		return dir;
	}
	
    //isShooting method for returning boolean
	public boolean isShooting()
	{
		return playerInput.isShoot();
	}
	
	public void move()
	{
		super.move();
	}

} //end of Player class
