package Entity;

import java.util.Random;

import BerzerkPKG.SpriteAnimation;
import javafx.animation.Animation;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Zombie2 extends Entity {
	
	//Zombie image file variables
    Image zombie2Image = new Image("images/Zombie2Right.png"); //default image for robot to Walk right
    Image zombie2Image1 = new Image("images/Zombie2Right.png"); //walk right
    Image zombie2Image2 = new Image("images/Zombie2Left.png"); //walk left
    Image zombie2Image3 = new Image("images/Zombie2Up.png"); //walk up
    Image zombie2Image4 = new Image("images/Zombie2Down.png"); //walk down
    //Image robotDyingImage = new Image("images/RobotDying.png"); //robot dying
	
    //Zombie variables for Sprite frames
    private static int zombie2Columns = 2; // number of columns in zombie sprite image
    private static int zombie2Count = 2;   // 4 frames of zombie walking up
    private int zombie2Offset_X = 0; // offset X of where the zombie sprite image starts in sprite sheet.
    private int zombie2Offset_Y = 0; // offset Y of where the zombie sprite image starts in sprite sheet.
    private static int zombie2Width = 25;  // width of zombie sprite
    private static int zombie2Height = 32; // height of zombie sprite
    
    //Boolean variables for entity facing direction
    boolean isWalkRight = true; //default for player to Walk right
    boolean isWalkLeft = false;
    boolean isWalkUp = false;
    boolean isWalkDown = false;
    boolean isShooting = false;
    
    //declaration of KeyHandler class
	double zombieMoveSpeed;
	Random dirInt = new Random();
	
    /** 
     * Zombie method to create zombie
     * Zombie constructor to invoke the Entity class 
     * parameters include:
     * @param pane
     * @param image
     * @param x
     * @param y
     * @param dx
     * @param dy
     * @param w
     * @param h
     * @param speed
     */
	
	public Zombie2(Pane pane, Image image, double x, double y, double dx, double dy, double w, double h, double zombieMoveSpeed) {
		
		super(pane, image, x, y, dx, dy, w, h);
		this.zombieMoveSpeed = zombieMoveSpeed;
		setMovable(true);
		this.imageView.setViewport(new Rectangle2D(zombie2Offset_X, zombie2Offset_Y, zombie2Width, zombie2Height));
        final Animation zombieAnimation = new SpriteAnimation(this.imageView,Duration.millis(700),zombie2Count, zombie2Columns, zombie2Offset_X, zombie2Offset_Y,zombie2Width, zombie2Height);
        zombieAnimation.setCycleCount(Animation.INDEFINITE);
        zombieAnimation.play();
	
	} //end of zombie method
	
public void zombie2RandomMove() {
		
		//add one zombie every direction
		int tempInt = dirInt.nextInt(30) + 1;
		
		//zombie right - default position
		if(tempInt == 1) {
			dx = zombieMoveSpeed; //move in +dx direction
			this.imageView.setImage(zombie2Image1);
			isWalkRight = true; 
			isWalkLeft = false;
		
		//zombie left
		} else if (tempInt == 2) {
			dx = -zombieMoveSpeed; //move in -dx direction
			this.imageView.setImage(zombie2Image2);
			isWalkUp = false;
			isWalkDown = true;
		
		//zombie up
		} else if(tempInt == 3 ) {
			dy = -zombieMoveSpeed; //move in -dy direction
			this.imageView.setImage(zombie2Image3);
			isWalkUp = true;
			isWalkDown = false;
		
		//zombie down
		} else if(tempInt == 4) {
			dy = zombieMoveSpeed; //move in +dy direction
			this.imageView.setImage(zombie2Image4);
			isWalkUp = false ;
			isWalkDown = true;
		
		} else {
			
			dx = 0;
			dy = 0;
			isWalkRight = true;
			isWalkLeft = false;
			isWalkUp = false;
			isWalkDown = false;
		
		} 
		
	} //end of randomZombieMove method
	
    //playZombieDeathAnimation method to show player dying image for collision logic
	public void playZombieDeathAnimation(Entity zombieEntity) {
		//this.imageView.setImage(zombieDyingImage);
		this.imageView.setViewport(new Rectangle2D(zombie2Offset_X, zombie2Offset_Y, zombie2Width, zombie2Height));
        final Animation playZombieDeathAnimation = new SpriteAnimation(
        		this.imageView, Duration.millis(200),
        		zombie2Columns, zombie2Columns,
        		zombie2Offset_X, zombie2Offset_Y,
        		zombie2Width, zombie2Height);
        playZombieDeathAnimation.setCycleCount(1);
        playZombieDeathAnimation.play();
        //System.out.println("Collided");
        //robotEntity.removeEntity();
		
	} //end of playRobotDeathAnimation
	
	public String getDirection()
	{
		String dir = "a";
		if(isWalkUp && isWalkRight)
			dir = "UP_RIGHT";
		else if(isWalkUp && isWalkLeft)
			dir = "UP_LEFT";
		else if(isWalkDown && isWalkRight)
			dir = "DOWN_RIGHT";
		else if(isWalkDown && isWalkLeft)
			dir = "DOWN_LEFT";
		else if(isWalkUp && (!isWalkRight && !isWalkLeft))
			dir = "UP";
		else if(isWalkDown && (!isWalkRight && !isWalkLeft))
			dir = "DOWN";
		else if(isWalkRight)
			dir = "RIGHT";
		else if(isWalkLeft)
			dir = "LEFT";
		return dir;
	}
	
	public boolean isShooting() {
		Random ranGen = new Random();
		int ranNum = ranGen.nextInt(200)+1;
		if(ranNum == 10)
		return true;
		return false;
	}
	
	public void move() {
		super.move();
	}

} //end of zombie 2 method

