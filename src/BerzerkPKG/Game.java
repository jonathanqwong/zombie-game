// Author: Jonathan Wong
// Assignment: A_12_Berzerk_Level_1
// Class: Java CIS 016

package BerzerkPKG;

import BerzerkPKG.KeyInputHandler;
import Entity.Entity;
import Entity.Otto;
import Entity.Player;
import Entity.Zombie;
import Entity.Zombie2;
import Entity.Walls;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;


import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Game extends Application {
	
	//CopyOnWriteArrayList for objects
	CopyOnWriteArrayList<Walls> wallsArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Walls> exitWallsArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Player> playerArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Zombie> zombiesArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Zombie2> zombies2Array = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Otto> ottoArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Entity> playerShootArray = new CopyOnWriteArrayList<>();
	CopyOnWriteArrayList<Entity> robotShootArray = new CopyOnWriteArrayList<>();
	
	//ArrayList for sound files
	ArrayList<AudioClip> playerSounds = new ArrayList<>();
 	ArrayList<AudioClip> zombieSounds = new ArrayList<>();
	
 	//Image files
	Image maze = new Image("images/bg.png");
	ImageView mazeImageView = new ImageView(maze);
	Image winImage = new Image("images/win.png");
	ImageView winImageView = new ImageView(winImage);
	Image loseImage = new Image("images/dead.png");
	ImageView loseImageView = new ImageView(loseImage);
	Image playerImage = new Image("images/WalkRight.png");
	Image playerDyingImage = new Image("images/Dying.png");
	Image zombieImage = new Image("images/ZombieLeft.png");
	Image zombie2Image = new Image("images/Zombie2Left.png");
	Image bulletRight = new Image("images/Bullet_Right.png");
	Image bulletLeft = new Image("images/Bullet_Left.png");
	Image bulletUp = new Image("images/Bullet_Up.png");
	Image bulletDown = new Image("images/Bullet_Down.png");
	Image bulletDiagonal1 = new Image("images/Bullet_Diagonal1.png");
	Image bulletDiagonal2 = new Image("images/Bullet_Diagonal2.png");
	Image zombieBlood = new Image("images/Zombie_Blood.png");
	Image ottoImage = new Image("images/Biohazard.png");	
	
	//Font file
	Font atariFont = Font.loadFont(getClass().getResourceAsStream("/fonts/Atari.ttf"), 35);	
 	
	//Objects in pane 
	Pane wallPane = new Pane();
	Pane playerPane = new Pane();
	Pane playerShootPane = new Pane();
	Pane playerDyingPane = new Pane();
	Pane robotPane = new Pane();
	Pane robotShootPane = new Pane();
	Pane winPane = new Pane();
	Pane losePane = new Pane();
	Pane scorePane = new Pane();
	Pane livesPane = new Pane();

	//Named constants
	static final int CANVAS_WIDTH = 1080;
	static final int CANVAS_HEIGHT = 690;
	public static final String TITLE = "Team Z Game";
	
 	//variable declaration
	boolean isAlive = false;
	boolean isDead = false;
	private int frames = 70;
	private int tick = 0;
	

	static String scoreTitle = "Score: " ;
	private int score = 0;
	String scoreBoard = scoreTitle + Integer.toString(score);
	Text scoreBoardText = new Text();
	
	static String livesTitle = "Lives: " ;
	private int lives = 3;
	String livesBoard = livesTitle + Integer.toString(lives);
	Text livesBoardText = new Text();
			
	@Override
	public void start(Stage primaryStage) {
		
		//initGame method invoked with game settings
		initGame(primaryStage);
		
		//Game play loop - put all arrays in gameLoop 
		AnimationTimer gameLoop = new AnimationTimer() {
			
			@Override
			public void handle(long timer) {
				
				playerArray.forEach(Entity -> Entity.playerInputMove());
				playerArray.forEach(Entity -> Entity.move());
				
				if(timer%5==0)
					zombiesArray.forEach(Entity -> Entity.zombieRandomMove());	
					zombiesArray.forEach(Entity -> Entity.move());
					zombies2Array.forEach(Entity -> Entity.zombie2RandomMove());	
					zombies2Array.forEach(Entity -> Entity.move());
					playerArray.forEach(Entity -> playerInputShoot(Entity));
					zombiesArray.forEach(Entity -> robotShoot(Entity));
					playerShootArray.forEach(Entity -> Entity.move());	
					robotShootArray.forEach(Entity -> Entity.move());	
				
				//Otto Timer and Movement
				if(tick >= 800){
					if(tick % 50 == 0)
					ottoArray.forEach(Entity -> Entity.ottoChase(playerArray.get(0)));
					ottoArray.forEach(Entity -> Entity.move());
				}
				
				//Updates Score/Lives
				scoreBoard = scoreTitle + Integer.toString(score);
				scoreBoardText.setText(scoreBoard);	
				livesBoard = livesTitle + Integer.toString(lives);
				livesBoardText.setText(livesBoard);
			
				//Check to see if player is alive/dead and change scene
				if(isAlive){
					winScreen(primaryStage);
					this.stop();
				}
				if(isDead){
					loseScreen(primaryStage);
					this.stop();
				}
				
				//invoke setCollisionLogic
				setCollisionLogic();
				
				//invoke playSoundEffects
				playSoundEffect();
				
				frames++;
				tick++;
			}
		};
		gameLoop.start();	
	} //end of start
	
	
	/**
	 ** initGame method - sets the Game up with score, stage
	 ** 
	 **/
	public void initGame(Stage primaryStage){
		//score
		scoreBoardText.setFont(atariFont);
		scoreBoardText.setFill(Color.rgb(254, 235, 66));
		scoreBoardText.setX(240);
		scoreBoardText.setY(535);
		scoreBoardText.setText(scoreBoard);
        scorePane.getChildren().add(scoreBoardText);
        
        //lives   
        livesBoardText.setFont(atariFont);
		livesBoardText.setFill(Color.rgb(254, 235, 66));
		livesBoardText.effectProperty();
		livesBoardText.setX(240);
		livesBoardText.setY(560);
		livesBoardText.setText(livesBoard);
        livesPane.getChildren().add(livesBoardText);
        
		//objects go into the stage
		Group root = new Group();
		root.getChildren().addAll(mazeImageView,wallPane,playerPane,playerShootPane,robotPane,robotShootPane,scorePane,livesPane);
		
		//scene properties inside Stage
		Scene scene = new Scene(root, CANVAS_WIDTH, CANVAS_HEIGHT, Color.BLACK);
		primaryStage.setTitle(TITLE);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		//sets up the player
		KeyInputHandler playerInput = new KeyInputHandler(scene);
		playerInput.addListeners();
		Player player = new Player(playerPane, playerImage, 110, 450, 0, 0, 26, 32, 3, playerInput);
		playerArray.add(player);
		
		//sets up Wall for the game environment
		drawWalls(root);
		
		//sets up the Robot and Otto
		initRobots();
		initSounds();
		
	}
	
	public void winScreen(Stage primaryStage)
	{
		winPane.getChildren().add(winImageView);
		Scene scene = new Scene(winPane, CANVAS_WIDTH, CANVAS_HEIGHT);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void loseScreen(Stage primaryStage)
	{	
		if (lives == 0) 
			losePane.getChildren().add(loseImageView);
			Scene scene = new Scene(losePane, CANVAS_WIDTH, CANVAS_HEIGHT);
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	/**
	 ** drawWalls method - sets the Game up with walls
	 ** Put walls into an  arrayList
	 ** parameters(pane, image, x, y, dx, dy, w, h)
	 **/
	public void drawWalls(Group givenRoot) {
		
		//Total of 19 walls

		//Upper Walls
		Walls wall1 = new Walls(wallPane, null, 0, 0, 0, 0, 565, 110); //top-left corner
		Walls wall2 = new Walls(wallPane, null, 275, 110, 0, 0, 180, 15); //top-left middle
		Walls wall3 = new Walls(wallPane, null, 0, 110, 0, 0, 130, 240); //top-left middle corner
		Walls wall4 = new Walls(wallPane, null, 0, 380, 0, 0, 55, 300); //bottom-left corner
		Walls wall5 = new Walls(wallPane, null, 650, 0, 0, 0, 430, 125); //top-right corner
		Walls wall6 = new Walls(wallPane, null, 650, 150, 0, 0, 130, 70); //top-right left corner
		Walls wall7 = new Walls(wallPane, null, 650, 205, 0, 0, 75, 15); //top-right left corner
		Walls wall8 = new Walls(wallPane, null, 1040, 150, 0, 0, 50, 150); //top-right right corner
		
		//Bottom Walls
		Walls wall9 = new Walls(wallPane, null, 150, 620, 0, 0, 400, 55); //bottom-left left
		Walls wall10 = new Walls(wallPane, null, 240, 495, 0, 0, 315, 150); //bottom-left middle 
		Walls wall11 = new Walls(wallPane, null, 385, 475, 0, 0, 150, 30); //bottom-left middle 
		Walls wall12 = new Walls(wallPane, null, 550, 475, 0, 0, 120, 75); //bottom-left right
		Walls wall13 = new Walls(wallPane, null, 630, 640, 0, 0, 230, 55); //bottom-middle middle
		Walls wall14 = new Walls(wallPane, null, 805, 475, 0, 0, 155, 75); //bottom-right left
		Walls wall15 = new Walls(wallPane, null, 950, 395, 0, 0, 120, 300); //bottom-right right
		
		//Middle Walls
		Walls wall16 = new Walls(wallPane, null, 230, 230, 0, 0, 80, 90); //middle-1
		Walls wall17 = new Walls(wallPane, null, 390, 225, 0, 0, 95, 85); //middle-2
		Walls wall18 = new Walls(wallPane, null, 445, 300, 0, 0, 80, 50); //middle-3
		Walls wall19 = new Walls(wallPane, null, 590, 310, 0, 0, 140, 60); //middle-4
		
		//Total of 3 exits
		Walls exit1 = new Walls(wallPane, null, 565, 0, 0, 0, 80, 5);
		Walls exit2 = new Walls(wallPane, null, 1070, 290, 0, 0, 5, 80);
		Walls exit3 = new Walls(wallPane, null, 550, 680, 0, 0, 80, 5);
		Walls exit4 = new Walls(wallPane, null, 860, 680, 0, 0, 80, 5);
		
		//Add walls/exits to array
		wallsArray.add(wall1);
		wallsArray.add(wall2);
		wallsArray.add(wall3);
		wallsArray.add(wall4);
		wallsArray.add(wall5);
		wallsArray.add(wall6);
		wallsArray.add(wall7);
		wallsArray.add(wall8);
		wallsArray.add(wall9);
		wallsArray.add(wall10);
		wallsArray.add(wall11);
		wallsArray.add(wall12);
		wallsArray.add(wall13);
		wallsArray.add(wall14);
		wallsArray.add(wall15);
		wallsArray.add(wall16);
		wallsArray.add(wall17);
		wallsArray.add(wall18);
		wallsArray.add(wall19);
		exitWallsArray.add(exit1);
		exitWallsArray.add(exit2);
		exitWallsArray.add(exit3);
		exitWallsArray.add(exit4);
		
	} //end of drawWall method
	
	/**
	 ** initZombies method - sets the Game up with zombies
	 ** Put robots into an  arrayList
	 ** parameters(pane, image, x, y, dx, dy, w, h, speed)
	 **/
	public void initRobots() {
		
		//Total of 12 zombies
		
		//6 zombie
		Zombie zombie1 = new Zombie(robotPane, zombieImage, 340, 360, 0, 0, 24, 30, .8);
		Zombie zombie2 = new Zombie(robotPane, zombieImage, 500, 150, 0, 0, 24, 30, .8);	
		Zombie zombie3 = new Zombie(robotPane, zombieImage, 575, 219, 0, 0, 24, 30, .8);
		Zombie zombie4 = new Zombie(robotPane, zombieImage, 715, 420, 0, 0, 24, 30, .8);
		Zombie zombie5 = new Zombie(robotPane, zombieImage, 700, 555, 0, 0, 24, 30, .8);
		Zombie zombie6 = new Zombie(robotPane, zombieImage, 850, 585, 0, 0, 24, 30, .8);
		
		//6 zombie2
		Zombie2 zombie7 = new Zombie2(robotPane, zombie2Image, 475, 410, 0, 0, 24, 30, 1.5);
		Zombie2 zombie8 = new Zombie2(robotPane, zombie2Image, 690, 385, 0, 0, 24, 30, 1.5);
		Zombie2 zombie9 = new Zombie2(robotPane, zombie2Image, 180, 160, 0, 0, 24, 30, 1.5);
		Zombie2 zombie10 = new Zombie2(robotPane, zombie2Image, 315, 425, 0, 0, 24, 30, 1.5);
		Zombie2 zombie11 = new Zombie2(robotPane, zombie2Image, 940, 340, 0, 0, 24, 30, 1.5);
		Zombie2 zombie12 = new Zombie2(robotPane, zombie2Image, 890, 250, 0, 0, 24, 30, 1.5);
		
		//1 Otto
		Otto otto = new Otto(robotPane, ottoImage, -50, -50, 0, 0, 35, 35, 1.5);
		
		//add robots and otto to array list
		zombiesArray.add(zombie1);
		zombiesArray.add(zombie2);
		zombiesArray.add(zombie3);
		zombiesArray.add(zombie4);
		zombiesArray.add(zombie5);
		zombiesArray.add(zombie6);
		zombies2Array.add(zombie7);
		zombies2Array.add(zombie8);
		zombies2Array.add(zombie9);
		zombies2Array.add(zombie10);
		zombies2Array.add(zombie11);
		zombies2Array.add(zombie12);
		ottoArray.add(otto);
	
	} //end of setRobots method
	
	/**
	 ** playerInputShoot method - sets up player with shooting
	 ** Player shoots in 8 directions
	 ** Put shot into an  arrayList
	 **/
	public void playerInputShoot(Player player){
		
		if(player.isShooting() && (frames >= 70)) {
			
			Random ranGen = new Random();
			int totalPlayerSoundEffect = ranGen.nextInt(2);
			String dir = player.getDirection();
			
			//shoots in 8 direction
			if(dir == "UP_RIGHT")
			{
				Entity shot1 = new Entity(
						playerShootPane, 
						bulletDiagonal1, 
						player.getX()+37, 
						player.getY()-44, 4, -4, 24, 24);
				playerShootArray.add(shot1);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			if(dir == "UP_LEFT")
			{
				Entity shot2 = new Entity(
						playerShootPane, 
						bulletDiagonal2,
						player.getX()-37, 
						player.getY()-44,
						-4, -4, 24, 24);
				playerShootArray.add(shot2);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "DOWN_RIGHT")
			{
				Entity shot3 = new Entity(
						playerShootPane, 
						bulletDiagonal2, 
						player.getX()+37, 
						player.getY()+10,
						4, 4, 24, 24);
				playerShootArray.add(shot3);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "DOWN_LEFT")
			{
				Entity shot4 = new Entity(
						playerShootPane, 
						bulletDiagonal1, 
						player.getX()-37, 
						player.getY()+10, 
						-4, 4, 24, 24);
				playerShootArray.add(shot4);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "UP")
			{
				Entity shot5 = new Entity(
						playerShootPane, 
						bulletUp, 
						player.getX(), 
						player.getY()-44, 
						0, -4, 24, 24);
				playerShootArray.add(shot5);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "DOWN")
			{
				Entity shot6 = new Entity(
						playerShootPane, 
						bulletDown, 
						player.getX(), 
						player.getY()+44, 
						0, 4, 24, 24);
				playerShootArray.add(shot6);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "RIGHT")
			{
				Entity shot7 = new Entity(
						playerShootPane, 
						bulletRight,
						player.getX()+37, 
						player.getY(), 
						4, 0, 24, 24);
				playerShootArray.add(shot7);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			else if(dir == "LEFT")
			{
				Entity shot8 = new Entity(
						playerShootPane,
						bulletLeft,
						player.getX()-37,
						player.getY(),
						-4, 0, 24, 24);
				playerShootArray.add(shot8);
				playerSounds.get(totalPlayerSoundEffect).play();
				frames = 0;
			}
			System.out.println("SHOOTING");
		}
	} //end of playerShoot method
	
	/**
	 ** robotShoot method - sets up robot with shooting randomly
	 ** Robot shoots in 8 directions
	 ** Each shot is instantiated from Entity object, 
	 ** Put shot into an  arrayList
	 **/
	public void robotShoot(Zombie robot) {
		if(robot.isShooting() && (frames >= 70)) {
			
			String dir = robot.getDirection();
			
			Random ranGen = new Random();
			int ranNum = ranGen.nextInt(2);
			
			if(dir == "UP_RIGHT")
			{
				Entity shot9 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX()+37, 
						robot.getY()-44, 
						2, -2, 24, 24);
				robotShootArray.add(shot9);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "UP_LEFT")
			{
				Entity shot10 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX()-37, 
						robot.getY()-44, 
						-2, -2, 24, 24);
				robotShootArray.add(shot10);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "DOWN_RIGHT")
			{
				Entity shot11 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX()+37, 
						robot.getY()+10, 
						2, 2, 24, 24);
				robotShootArray.add(shot11);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "DOWN_LEFT")
			{
				Entity shot12 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX()-37, 
						robot.getY()+10, 
						-2, 2, 24, 24);
				robotShootArray.add(shot12);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "UP")
			{
				Entity shot13 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX(), 
						robot.getY()-44, 
						0, -2, 24, 24);
				robotShootArray.add(shot13);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "DOWN")
			{
				Entity shot14 = new Entity(
						robotShootPane, 
						zombieBlood,
						robot.getX(), 
						robot.getY()+44,
						0, 2, 24, 24);
				robotShootArray.add(shot14);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "RIGHT")
			{
				Entity shot15 = new Entity(
						robotShootPane, 
						zombieBlood,
						robot.getX()+37, 
						robot.getY(), 
						2, 0, 24, 24);
				robotShootArray.add(shot15);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			else if(dir == "LEFT")
			{
				Entity shot16 = new Entity(
						robotShootPane, 
						zombieBlood, 
						robot.getX()-37, 
						robot.getY(), 
						-2, 0, 24, 24);
				robotShootArray.add(shot16);
				zombieSounds.get(ranNum).play();
				frames = 0;
			}
			
			System.out.println("SHOOTING");
		}
	} //end of robotShoot Method
	
	
	/**
	 ** setCollisions method - sets the Game up with collision logic
	 **/
	public void setCollisionLogic() {
		
		//In this for loop, the temp holds the current value from the array
		
		//Player Collision Logic - player dies from robots, Otto, shot, and walls
		for(Player tempPlayer : playerArray) {
			
			//Walls-to-player
			for(Walls tempWall : wallsArray) {
				if(tempPlayer.isCollidingWithWall(tempWall)) {
					tempPlayer.playPlayerDeathAnimation(tempPlayer);

					
					lives = lives - 1;
				} 
			}
				
			//Walls-to-player
			for(Walls tempExit : exitWallsArray) {
				if(tempPlayer.isCollidingWithWall(tempExit)) {
					isAlive = true;
				}
			}
			
			//Zombie-to-player
			for(Zombie tempRobots : zombiesArray) {
				if(tempPlayer.isCollidingWithRobots(tempRobots)) {
					tempPlayer.playPlayerDeathAnimation(tempPlayer);
					try {
					    Thread.sleep(1250); 
					} catch(InterruptedException ex) {
					    Thread.currentThread().interrupt();
					}
					//tempPlayer.removeEntity();
					//playerArray.remove(tempPlayer);
					lives -= 1;
				}
			}
			
			//RobotShoot-to-player
			for(Entity tempRobotShoot : robotShootArray) {
				if(tempPlayer.isCollidingWithRobots(tempRobotShoot)) {
					tempPlayer.playPlayerDeathAnimation(tempPlayer);
					try {
					    Thread.sleep(1250); 
					} catch(InterruptedException ex) {
					    Thread.currentThread().interrupt();
					}
					//tempPlayer.removeEntity();
					//playerArray.remove(tempPlayer);
					lives -= 1;
				}
			}	
			
			//Otto-to-player
			for(Otto tempOtto : ottoArray) {
				if(tempPlayer.isCollidingWithRobots(tempOtto)) {
					tempPlayer.playPlayerDeathAnimation(tempPlayer);
					try {
					    Thread.sleep(1250); 
					} catch(InterruptedException ex) {
					    Thread.currentThread().interrupt();
					}
					//tempPlayer.removeEntity();
					lives -= 1;
				}
			}	
			
			
		} //end of Player Collision Loop
		
		//Zombie Collision Logic - zombie dies on shot and wall
		for(Zombie tempRobot : zombiesArray) {
			
			//Shot-to-robot
			for(Entity tempShot : playerShootArray) {

				if(tempShot.isCollidingWithRobots(tempRobot)) {
					tempRobot.playZombieDeathAnimation(tempRobot);
					try {
					    Thread.sleep(200); 
					} catch(InterruptedException ex) {
					    Thread.currentThread().interrupt();
					}
					tempRobot.removeEntity();
					tempShot.removeEntity();
					playerShootArray.remove(tempShot);
					zombiesArray.remove(tempRobot);
					score +=100;
				}
			}
			
		}  //end of Zombie Collision Loop
		
		//Zobmie2 Collision Logic - zombie dies on shot and wall
		for(Zombie2 tempRobot2 : zombies2Array) {
			
			//Shot-to-robot
			for(Entity tempShot : playerShootArray) {

				if(tempShot.isCollidingWithRobots(tempRobot2)) {
					tempRobot2.playZombieDeathAnimation(tempRobot2);
					try {
					    Thread.sleep(200); 
					} catch(InterruptedException ex) {
					    Thread.currentThread().interrupt();
					}
					tempRobot2.removeEntity();
					tempShot.removeEntity();
					playerShootArray.remove(tempShot);
					zombies2Array.remove(tempRobot2);
					score +=100;
				}
			}
			
		}  //end of Zombie2 Collision Loop
		
		//Wall Collision Logic -  shot dies on walls, other shots, player, enemy
		for(Walls tempWall : wallsArray) {
			
			//(player)Shot-to-Wall
			for(Entity tempPlayerShot : playerShootArray) {
				if(tempPlayerShot.isCollidingWithWall(tempWall)){
					tempPlayerShot.removeEntity();
					playerShootArray.remove(tempPlayerShot);
				}
			}
			
			//(robot)Shot-to-Wall
			for(Entity tempRobotShot : robotShootArray){
				if(tempRobotShot.isCollidingWithWall(tempWall)){
					tempRobotShot.removeEntity();
					robotShootArray.remove(tempRobotShot);
				}
			}
			
		}
		
	} //end of setCollisions method
	
	/**
	 **initSounds Method gets sound file
	 **/
	public void initSounds() {
		String addSoundEffect;
		
		//Player sounds
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/Blast_1.mp3").getFile();
		playerSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/Blast_2.mp3").getFile();
		playerSounds.add(new AudioClip(addSoundEffect));
		
		//Zombie sounds
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-1.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-2.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-3.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-4.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-5.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-6.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-7.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-8.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-15.wav").getFile();
		zombieSounds.add(new AudioClip(addSoundEffect));
		addSoundEffect = "file:" + Game.class.getClassLoader().getResource("sounds/zombie-16.wav").getFile();
	}
	
	/**
	 ** playSoundEffect Method shuffle zombieFiles
	 **/
	private void playSoundEffect(){
		
		Random randGen = new Random();
		int totalSoundEffects = zombieSounds.size();
		int index = randGen.nextInt(totalSoundEffects);
		
		if (tick % 250 == 0) {
			if(zombiesArray.size() > 0){
				zombieSounds.get(index).play();
			}
		}
	}
		
	
	//main method
	public static void main(String[] args) {
		launch(args);
	}
	

} //end of Game class


	
